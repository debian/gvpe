gvpe (3.1-3) UNRELEASED; urgency=medium

  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Re-export upstream signing key without extra signatures.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 04 Dec 2019 06:13:28 +0000

gvpe (3.1-2) unstable; urgency=medium

  [ Unit 193 ]
  * d/copyright: Remove getopt.(c,h) references.
  * d/copyright, d/watch: Use https where possible.
  * d/control: Build-depend on pkg-config. (Closes: #911968)
  * d/gvpe.default, d/init.d, d/rules:
    - Replace the enable/disable options, use --no-enable instead.
  * d/u/signing-key.asc, d/watch: Check tarball against upstream's signing key.

 -- TANIGUCHI Takaki <takaki@debian.org>  Sat, 27 Oct 2018 13:13:31 +0900

gvpe (3.1-1) unstable; urgency=medium

  * New upstream version 3.1
  * debian/patches/*: Drop openssl1.1 patches.
  * Bump Stanrads-Version to 4.2.1

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 26 Oct 2018 11:43:08 +0900

gvpe (3.0-2) unstable; urgency=medium

  [ Unit 193 ]
  * d/changelog, d/copyright: Remove trailing white spaces.
  * d/control: Change 'priority' to 'optional'.
  * d/control: Wrap and sort build-depends.
  * d/control: Update Vcs-* fields for salsa.d.o.
  * d/compat, d/control: Bump dh compat to 11.
  * d/control: Drop B-D on autotools-dev, dh-autoreconf and info.
  * d/rules: Drop explicit inclusion of autoreconf.
  * d/copyright: Use https in 'format' field.
  * d/p/openssl1.1.patch: Add patch from Faux to fix build with openssl 1.1.
    (Closes: #851076)
  * d/control: Adjust openssl B-D to 'libssl-dev'.
  * d/p/bundled_lib.patch: Don't include local copies of getopt, causes FTBFS.
    (Closes: #887662)
  * Update Standards-Version to 4.1.3.

  [ TANIGUCHI Takaki ]
  * Bump Stanrads-Version to 4.1.4
  * Move vcs repository under debian-group

 -- TANIGUCHI Takaki <takaki@debian.org>  Sun, 13 May 2018 10:28:55 +0900

gvpe (3.0-1) unstable; urgency=medium

  * New upstream.
  * Merge from https://mentors.debian.net/package/gvpe.

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 02 Feb 2017 21:38:52 +0900

gvpe (3.0-0vanir1) unstable; urgency=medium

  * Imported Upstream version 3.0
    - Drop patch applied upstream.
  * d/control:
    - Update Standards-Version to 3.9.8.
    - Vcs-Git: Use a https URL here too.
    - Build-dep on libssl-dev (<< 1.1) | libssl1.0-dev.
    - Depend upon lsb-base for the init script.
  * d/rules: No longer force re-generation of docs.

 -- Unit 193 <unit193@ubuntu.com>  Sat, 12 Nov 2016 21:08:04 -0500

gvpe (2.25+cvs20150424-0vanir1) unstable; urgency=medium

  * New upstream snapshot.
    - Remove all patches.
  * d/p/manpage_format.patch: Fix a syntax error in the pod file.
  * d/rules:
    - Drop useless dh_auto_install override.
    - dh_clean doc/gvpe.conf.5, doc/gvpe.info and doc/gvpe.osdep.5 too.
  * d/watch: Remove dh-make template text.
  * d/control:
    - VCS-*, make canonical, use cgit interface and https.
    - Update Standards-Version to 3.9.6.
    - build-depends: info → texinfo.
  * d/control, d/compat: Update debhelper compat to 9.
  * d/copyright: lib/libgettext.h → lib/gettext.h

 -- Unit 193 <unit193@ubuntu.com>  Wed, 24 Jun 2015 16:57:08 -0400

gvpe (2.25-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Build-depend on zlib1g-dev. (Closes: #850983)

 -- Adrian Bunk <bunk@debian.org>  Thu, 26 Jan 2017 00:18:25 +0200

gvpe (2.25-3) unstable; urgency=medium

  * debian/control: B-D replaced by libssl1.0-dev (Closes: #828336)
  * Bump debian policy to 3.9.8
  * debian/control: Add lsb-base to Depends
  * debian compat 9

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 13 Dec 2016 17:39:01 +0900

gvpe (2.25-2) unstable; urgency=medium

  * Build with dh-autoreconf. (Closes: #727385)
  * Bump Standards-Version to 3.9.5 (with no changes).

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 04 Feb 2014 18:11:09 +0900

gvpe (2.25-1) unstable; urgency=low

  * Imported Upstream version 2.25
  * debian/patches: Remove upstream merged patches..
  * debian/patches/fix_gvpe_conf_5: fix an empty man page.
  * debian/control: Bump Standards-Version to 3.9.4 (with no changes).
  * debian/copyright: Fixed license name.

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 19 Aug 2013 17:43:11 +0900

gvpe (2.24-2) unstable; urgency=low

  * debian/control: Add Vcs-* fields.
  * debian/gvpe.default: rename parameter DAEMON_OPTS to DAEMON_ARGS.
    (Closes: #647809)

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 15 Nov 2011 10:58:19 +0900

gvpe (2.24-1) unstable; urgency=low

  * New upstream.
  * patches/fix_openssl_m4, fix_configure: Fixed to fail configure
    (Closes: #621897, #622036)
  * info: Removed .
  * Standards-Version: 3.9.2

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 21 Jun 2011 18:11:48 +0900

gvpe (2.22-1) unstable; urgency=low

  * Initial release (Closes: #515307)

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 24 Jan 2011 10:26:07 +0900
